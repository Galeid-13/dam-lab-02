import React, {Component} from 'react';
import {
  StyleSheet, 
  TouchableOpacity, 
  Text, 
  View, 
  TextInput,
  Image
} from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      passValue: '',
    };
  }

  changeTextInput = text => {
    this.setState({inputValue: text});
  };

  changePasswordInput = text => {
    this.setState({passValue: text});
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.text}>
          <Text style={{fontSize: 20,}}>GitHub</Text>
        </View>

        <Image source={require('./img/logoGit.png')} style={styles.image}/>

        <View style={styles.content}>
          <TextInput 
            style={{height:50, 
              borderColor: 'gray', 
              borderWidth: 1, 
              marginHorizontal: 100}}
            onChangeText={text => this.changeTextInput(text)}
            value={this.state.inputValue}
          />

          <TextInput 
            secureTextEntry={true}
            style={{height:50, 
              borderColor: 'gray', 
              borderWidth: 1, 
              marginTop: 10, 
              marginHorizontal: 100}}
            onChangeText={text => this.changePasswordInput(text)}
            value={this.state.passInputValue}
          />

          <TouchableOpacity style={styles.button}>
            <Text>Iniciar Sesión</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  text: {
    flex: .1,
    alignItems: 'center',
    padding: 20,
  },
  button: {
    marginHorizontal: 150,
    marginTop: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
  content:{
    flex: 2.5,
    marginTop: 20,
  },
  image:{
    marginHorizontal: 80,
    flex: 1,
    width: 300,
    height: 200,
    resizeMode: 'center',
  }
});

